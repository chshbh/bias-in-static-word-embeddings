import torch
import numpy as np
import torchtext
import pandas as pd
import seaborn as sns

# import from main

kek = np.load(PATH + "model_result.npy")
k = pd.DataFrame(kek.T, columns=["Training Loss", "Validation Loss"])
k = k.melt(var_name="Type", value_name="Loss", ignore_index=False)
k["Epochs"] = k.index
k.reset_index(inplace=True)
g = sns.lineplot(x="Epochs", y="Loss", data=k, hue="Type")
g.set(ylim=(0, 20))
