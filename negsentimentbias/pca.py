import torch
import torchtext
import torchmetrics as T
import numpy as np
from ast import literal_eval
import warnings
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from lib import clean_words
from utils import Classifier


# silence warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=UserWarning)

# gpu(cuda) support
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
torch.backends.cudnn.benchmark = True

# PATH = "biasedweights/"
PATH = "debiasedweights/"

# EMBEDDING_PATH = "glove.840B.300d.txt"
# EMBEDDING_PATH = "GoogleNews-vectors-negative300.txt"
EMBEDDING_PATH = "GoogleNews-vectors-negative300-hard-debiased.txt"

# loading word embeddings
embed = torchtext.vocab.Vectors(EMBEDDING_PATH)

# loading negative words
neg_word = np.loadtxt("negsentimentbias/negative-words.txt", dtype="U32")
# loading positive words
pos_word = np.loadtxt("negsentimentbias/positive-words.txt", dtype="U32")

# cleaning dataset vocab with respect to given word embedding vocab
clean_neg_words = clean_words(neg_word, embed)
clean_pos_words = clean_words(pos_word, embed)

kek = lambda x: embed.get_vecs_by_tokens(list(x)).type(torch.float32)
k = lambda x: clean_words(np.array((x)), embed)

seeds_df = pd.read_json("negsentimentbias/gathered_seeds.json", orient="records")
seeds = seeds_df["Seeds"].apply(literal_eval).to_frame()
seeds["Length"] = seeds["Seeds"].apply(lambda list: len(list))
seed = seeds[seeds["Length"] > 0].copy()
seed.sort_values(by="Length", ascending=False, inplace=True)
# seed.reset_index(inplace=True)

# # bolukbasi gender
# female = seed.at[34,"Seeds"]
# male = seed.at[35,"Seeds"]

# zhao gn gender
female = seed.iloc[5]["Seeds"]
male = seed.iloc[6]["Seeds"]

# bolukbasi
gender_specific = seed.iloc[0]["Seeds"]
# zhao
gender_preserving = seed.iloc[1]["Seeds"]

neg = kek(clean_neg_words)
pos = kek(clean_pos_words)
female = kek(k(female))
male = kek(k(male))
# bolukbasi = kek(k(gender_specific))
# kaneko = kek(k(gender_preserving))

# X = torch.concat((neg,pos,female,male,bolukbasi,kaneko))
X = torch.concat((neg, pos, female, male))
U, S, V = torch.pca_lowrank(X)
pc = torch.matmul(X, V[:, :2])
data = pd.DataFrame({"X": pc[:, 0], "Y": pc[:, 1]})

data["Sentiment"] = (
    ["Negative"] * (neg.shape[0])
    + ["Positive"] * (pos.shape[0])
    + ["Female"] * (female.shape[0])
    + ["Male"] * (male.shape[0])
)
# + ["Specific"]*(bolukbasi.shape[0]) + ["Neutral"]*(kaneko.shape[0])
g = sns.relplot(x=data.X, y=data.Y, data=data, hue="Sentiment", height=5, aspect=1.5)
g.figure.savefig(PATH + "pca.png")
