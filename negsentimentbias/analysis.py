import torch
import torchtext
import torchmetrics as T
import numpy as np
from ast import literal_eval
import warnings
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from utils import Classifier
from lib import clean_words
import math


# silence warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=UserWarning)

# gpu(cuda) support
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
torch.backends.cudnn.benchmark = True


# PATH = "biasedweights/"
PATH = "debiasedweights/"

X_test = torch.load(PATH + "X_test.pt")
Y_test = torch.load(PATH + "Y_test.pt")

# load model
path = PATH + "model.pt"
model = Classifier(300)
model.load_state_dict(torch.load(path))
model.eval()

# EMBEDDING_PATH = "glove.840B.300d.txt"
EMBEDDING_PATH = "GoogleNews-vectors-negative300.txt"
# EMBEDDING_PATH = "GoogleNews-vectors-negative300-hard-debiased.txt"

# loading word embeddings
embed = torchtext.vocab.Vectors(EMBEDDING_PATH)


kek = lambda x: embed.get_vecs_by_tokens(list(x)).type(torch.float32)
k = lambda x: clean_words(np.array((x)), embed)

seeds_df = pd.read_json("negsentimentbias/gathered_seeds.json", orient="records")
seeds = seeds_df["Seeds"].apply(literal_eval).to_frame()
seeds["Length"] = seeds["Seeds"].apply(lambda list: len(list))
seed = seeds[seeds["Length"] > 0].copy()
seed.sort_values(by="Length", ascending=False, inplace=True)
# seed.reset_index(inplace=True)

# # bolukbasi gender
# female = seed.at[34,"Seeds"]
# male = seed.at[35,"Seeds"]

# zhao gn gender
female = seed.iloc[5]["Seeds"]
male = seed.iloc[6]["Seeds"]

# bolukbasi
gender_specific = seed.iloc[0]["Seeds"]
# zhao
gender_preserving = seed.iloc[1]["Seeds"]

female = k(female)
male = k(male)
bolukbasi = k(gender_specific)
kaneko = k(gender_preserving)

f_pred = torch.nn.Sigmoid()(model(kek(female)))
f = pd.DataFrame(f_pred.detach().numpy()).rename(columns={0: "Probability"})
f["Gender"] = "Female"

m_pred = torch.nn.Sigmoid()(model(kek(male)))
m = pd.DataFrame(m_pred.detach().numpy()).rename(columns={0: "Probability"})
m["Gender"] = "Male"


bolukbasi_pred = torch.nn.Sigmoid()(model(kek(bolukbasi)))
boluk = pd.DataFrame(bolukbasi_pred.detach().numpy()).rename(columns={0: "Probability"})
boluk["Gender"] = "Gendered"

kaneko_pred = torch.nn.Sigmoid()(model(kek(kaneko)))
kanek = pd.DataFrame(kaneko_pred.detach().numpy()).rename(columns={0: "Probability"})
kanek["Gender"] = "Neutral"

gender = pd.concat((m, f, boluk, kanek)).reset_index(drop=True)
hue_order = ["Neutral", "Gendered", "Male", "Female"]
# g.set(xlim=(0,1))

plot1 = lambda dataframe: sns.catplot(
    x="Gender", y="Probability", data=dataframe, hue="Gender", height=5, aspect=1.5
)
plot2 = lambda dataframe: sns.displot(
    x=dataframe.index,
    y="Probability",
    data=dataframe,
    hue="Gender",
    height=5,
    aspect=1.5,
)


g = sns.pointplot(
    x="Gender", y="Probability", data=gender, hue="Gender", hue_order=hue_order
)
g.set(ylim=(0, 1))

def sentiment_bias(p, q):
    return abs((-p+q).mean().item())    

p = torch.nn.LogSoftmax(0)(m_pred).detach()
q = torch.nn.LogSoftmax(0)(1 - m_pred).detach()
# q = np.array([1 / p.shape[0]] * p.shape[0])
p = p.reshape(p.shape[0])
q = q.reshape(p.shape[0])
print(sentiment_bias(p, q))

p = torch.nn.LogSoftmax(0)(f_pred).detach()
q = torch.nn.LogSoftmax(0)(1 - f_pred).detach()
# q = np.array([1 / p.shape[0]] * p.shape[0])
p = p.reshape(p.shape[0])
q = q.reshape(p.shape[0])
print(sentiment_bias(p, q))

p = torch.nn.LogSoftmax(0)(bolukbasi_pred).detach()
q = torch.nn.LogSoftmax(0)(1 - bolukbasi_pred).detach()
# q = np.array([1 / p.shape[0]] * p.shape[0])
p = p.reshape(p.shape[0])
q = q.reshape(p.shape[0])
print(sentiment_bias(p, q))

p = torch.nn.LogSoftmax(0)(kaneko_pred).detach()
q = torch.nn.LogSoftmax(0)(1 - kaneko_pred).detach()
# q = np.array([1 / p.shape[0]] * p.shape[0])
p = p.reshape(p.shape[0])
q = q.reshape(p.shape[0])
print(sentiment_bias(p, q))

religious = seed.iloc[28]["Seeds"]
rel = k(religious)
r_pred = torch.nn.Sigmoid()(model(kek(rel)))
p = torch.nn.Softmax(0)(r_pred).detach()
q = torch.nn.LogSoftmax(0)(1 - r_pred).detach()
# q = np.array([1 / p.shape[0]] * p.shape[0])
p = p.reshape(p.shape[0])
q = q.reshape(q.shape[0])
print(sentiment_bias(p, q))
