import torch
import numpy as np
import torchtext
from functools import partial
from math import ceil, floor


def unit_vector(vec: np.array) -> torch.tensor:
    """vec.div(norm(vec))"""
    vec = torch.tensor(vec, dtype=torch.float64)
    return vec.div(torch.linalg.norm(vec))


def clean_words(words: np.array, embed: torchtext.vocab.vectors.Vectors) -> np.array:
    """efficient set.intersection(a,b) where
    a is dataset lexicon, b is embedding lexicon"""
    return words[np.fromiter(map(lambda i: i in embed.stoi, words), dtype=bool)]


def jensen_shannon_divergence(p: torch.Tensor, q: torch.Tensor) -> torch.tensor:
    # JSD(P||Q)
    KL = torch.nn.KLDivLoss(reduction="batchmean", log_target=True)
    m = 0.5 * (p + q)
    return 0.5 * (KL(p, m) + KL(q, m))


def sampling(arr1: np.array, arr2: np.array, flag: bool) -> np.array:
    """
    Over sampling of the minority class or Under sampling of the majority class
    up sample if flag == True else down sample
    """
    if bool(flag):
        return np.random.choice(arr1, np.maximum(arr1.size, arr2.size), replace=True)
    else:
        return np.random.choice(arr1, np.minimum(arr1.size, arr2.size), replace=False)


def accuracy(y_true: torch.Tensor, y_prob: torch.Tensor) -> float:
    assert y_true.size() == y_prob.size()
    y_prob = y_prob > 0.5
    return (y_true == y_prob).sum().item() / y_true.size(0)


def random_split_data(data: np.array) -> torch.utils.data.dataset.Subset:
    """
    randomly splitting dataset into train + test + validation, outputs indices
    """
    dim = data.shape[0]
    idx = torch.arange(dim)
    train_idx, test_idx = floor(0.8 * dim), ceil(0.2 * dim)
    train_idx, validation_idx = floor(0.85 * train_idx), ceil(0.15 * train_idx)
    assert (
        train_idx + test_idx + validation_idx == dim
    ), "train-test-validation spliting"
    idx_train, idx_test, idx_validation = torch.utils.data.random_split(
        idx,
        [train_idx, test_idx, validation_idx],
        generator=torch.Generator().manual_seed(1101),
    )
    return idx_train, idx_test, idx_validation


def stratify(neg: np.array, pos: np.array) -> tuple:
    """split negative positive dataset to train, test, validation"""
    map_ = lambda index, split, array: array[split[index]]
    rsn = random_split_data(neg)
    tr_n, t_n, v_n = map_(0, rsn, neg), map_(1, rsn, neg), map_(2, rsn, neg)
    rsp = random_split_data(pos)
    tr_p, t_p, v_p = map_(0, rsp, pos), map_(1, rsp, pos), map_(2, rsp, pos)
    X_train = np.concatenate((tr_n, tr_p))
    X_test = np.concatenate((t_n, t_p))
    X_validate = np.concatenate((v_n, v_p))
    X = (X_train, X_test, X_validate)
    # labels
    Y_train_n, Y_train_p = torch.zeros((tr_n.shape[0], 1)), torch.ones(
        (tr_p.shape[0], 1)
    )
    Y_train = torch.concat((Y_train_n, Y_train_p)).type(torch.float64)
    Y_test_n, Y_test_p = torch.zeros((t_n.shape[0], 1)), torch.ones((t_p.shape[0], 1))
    Y_test = torch.concat((Y_test_n, Y_test_p)).type(torch.float64)
    Y_validate_n, Y_validate_p = torch.zeros((v_n.shape[0], 1)), torch.ones(
        (v_p.shape[0], 1)
    )
    Y_validate = torch.concat((Y_validate_n, Y_validate_p)).type(torch.float64)
    Y = (Y_train, Y_test, Y_validate)
    return tuple((X, Y))


# helper functions
down_sampling = partial(sampling, flag=False)
up_sampling = partial(sampling, flag=True)


if __name__ == "__main__":
    main()
