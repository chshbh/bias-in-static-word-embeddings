import torch
import numpy as np
import torchtext
from tqdm import trange

import warnings

from lib import clean_words, up_sampling, random_split_data, stratify
from utils import Classifier, Dataset

# silence warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=UserWarning)

# gpu(cuda) support
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
torch.backends.cudnn.benchmark = True


# EMBEDDING_PATH = "glove.840B.300d.txt"
# EMBEDDING_PATH = "GoogleNews-vectors-negative300.txt"
EMBEDDING_PATH = "GoogleNews-vectors-negative300-hard-debiased.txt"
# loading word embeddings
embed = torchtext.vocab.Vectors(EMBEDDING_PATH)
# PATH = "biasedweights/"
PATH = "debiasedweights/"

# loading negative words
neg_word = np.loadtxt("negsentimentbias/negative-words.txt", dtype="U32")
# loading positive words
pos_word = np.loadtxt("negsentimentbias/positive-words.txt", dtype="U32")


# cleaning dataset vocab with respect to given word embedding vocab
clean_neg_words = clean_words(neg_word, embed)
clean_pos_words = clean_words(pos_word, embed)

# oversampling
clean_neg_words_up = up_sampling(clean_neg_words, clean_pos_words)
clean_pos_words_up = up_sampling(clean_pos_words, clean_neg_words)


# split dataset into train, test, validation
X_words, Y = stratify(clean_neg_words_up, clean_pos_words_up)

kek = lambda x: embed.get_vecs_by_tokens(list(x)).type(torch.float32)
X_train, X_test, X_validation = kek(X_words[0]), kek(X_words[1]), kek(X_words[2])
Y_train, Y_test, Y_validation = Y[0], Y[1], Y[2]


torch.save(X_train, PATH + "X_train.pt")
torch.save(X_test, PATH + "X_test.pt")
torch.save(X_validation, PATH + "X_validation.pt")
torch.save(Y_train, PATH + "Y_train.pt")
torch.save(Y_test, PATH + "Y_test.pt")
torch.save(Y_validation, PATH + "Y_validation.pt")

# The DataLoader pulls instances of data from the Dataset
# (either automatically or with a sampler that you define), collects them in
# batches, and returns them for consumption by your training loop.
dataset_train = Dataset(X_train, Y_train)
dataset_test = Dataset(X_test, Y_test)
dataset_validation = Dataset(X_validation, Y_validation)
dataloader_train = torch.utils.data.DataLoader(
    dataset_train, batch_size=32, shuffle=True
)
dataloader_test = torch.utils.data.DataLoader(dataset_test, batch_size=32, shuffle=True)
dataloader_valid = torch.utils.data.DataLoader(
    dataset_validation, batch_size=32, shuffle=True
)


# invoke Classifier class with feature, output size
model = Classifier(300, 1)
# loss function
## choose bcewithlogitsloss to skip sigmoid function
## choose bceloss to get output in [0,1] range
loss_fn = torch.nn.BCEWithLogitsLoss()
# choose optimizer, adam probably best for our use case
optimizer = torch.optim.Adam(model.parameters(), lr=0.00146)


def train_loop(
    model: Classifier,
    dataloader: torch.utils.data.dataloader.DataLoader,
    loss_fn: torch.nn.modules.loss.BCEWithLogitsLoss,
    optimizer: torch.optim.Adam,
):
    """
    Below, we have a function that performs one training epoch.
    It enumerates data from the DataLoader,
    and on each pass of the loop does the following:
    1. Gets a batch of training data from the DataLoader
    2. Zeros the optimizer’s gradients
    3. Performs an inference - that is, gets predictions from the model for an input batch
    4. Calculates the loss for that set of predictions vs. the labels on the dataset
    5. Calculates the backward gradients over the learning weights
    6. Tells the optimizer to perform one learning step - that is,
        adjust the model’s learning weights based on the observed gradients for this batch,
        according to the optimization algorithm we chose
    7. It reports on the loss for every 100 batches.
    """
    model.train()
    size = len(dataloader.dataset)
    for batch, (X, y) in enumerate(dataloader):
        # Zero your gradients for every batch!
        optimizer.zero_grad()
        # Compute prediction and loss
        pred = model(X)
        loss = loss_fn(pred, y)
        # Backpropagation
        loss.backward()
        # Adjust learning weights
        optimizer.step()
        if batch % 64 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")
    return loss.item()


def validation_loop(
    model: Classifier,
    dataloader: torch.utils.data.dataloader.DataLoader,
    loss_fn: torch.nn.modules.loss.BCEWithLogitsLoss,
):
    model.eval()
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    loss = 0
    with torch.no_grad():
        for X, y in dataloader:
            pred = model(X)
            loss += loss_fn(pred, y).item()

    correct = (
        (
            (torch.nn.Sigmoid()(model(dataloader.dataset.words)) > 0.5)
            == dataloader.dataset.labels
        ).sum()
    ).item()
    loss /= num_batches
    correct = correct * 100 / size
    print(f"Test Error: \n Accuracy: {(correct):>0.1f}%, Avg loss: {loss:>8f} \n")
    return loss_fn(pred, y).item()


if __name__ == "__main__":
    epochs = 1000
    loss_train_list = list()
    loss_valid_list = list()
    for epoch in trange(epochs):
        loss_train_list.append(train_loop(model, dataloader_train, loss_fn, optimizer))
        loss_valid_list.append(validation_loop(model, dataloader_valid, loss_fn))

    torch.save(model.state_dict(), PATH + "model.pt")
    np.save(PATH + "model_result.npy", np.array((loss_train_list, loss_valid_list)))
