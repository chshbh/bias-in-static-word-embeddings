import torch
import torchmetrics as T
import numpy as np
import warnings
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from utils import Classifier


# silence warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=UserWarning)

# gpu(cuda) support
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
torch.backends.cudnn.benchmark = True

PATH = "biasedweights/"
# PATH = "debiasedweights/"

X_test = torch.load(PATH + "X_test.pt")
Y_test = torch.load(PATH + "Y_test.pt")


# load model
path = PATH + "model.pt"
model = Classifier(300)
model.load_state_dict(torch.load(path))
model.eval()

pred = torch.nn.Sigmoid()(model(X_test).detach())
truth = Y_test
t = truth.type(torch.LongTensor)
accuracy = T.Accuracy()(pred, t).item() * 100
f1_score = T.F1Score()(pred, t).item()
auroc = T.AUROC()(pred, t).item() * 100
fpr, tpr, thresholds = T.ROC()(pred, t)


def annotate(data, **kws):
    n = len(data)
    ax = plt.gca()
    ax.text(
        0.8,
        0.2,
        f"Accuracy {accuracy:.3f}%\n"
        f"AUROC    {auroc:.3f}%\n"
        f"F1 Score    {f1_score:.3f}\n",
        transform=ax.transAxes,
    )


df = pd.DataFrame({"TPR": tpr, "FPR": fpr})
g = sns.FacetGrid(
    data=df,
    height=5,
    aspect=1.5,
)
g.map_dataframe(sns.lineplot, x=df.columns[1], y=df.columns[0])
g.set_axis_labels(
    "False Positive Rate\n1 - Specificity", "True Positive Rate\nSensitivity"
)
g.map_dataframe(annotate)
i = [_ / 10 for _ in range(0, 11)]
g.map(sns.lineplot, x=i, y=i, alpha=0.1, color="black")
# g.set(xlim=(0,1))
# g.set(ylim=(0,1))
g.tight_layout()
g.savefig(PATH + "aoc.png")
