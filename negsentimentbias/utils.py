import torch
import numpy as np


class Classifier(torch.nn.Module):
    """binary classfier, linear + relu + linear + sigmoid"""

    def __init__(self, input_features: int, output: int = 1, p_dropout: float = 0.25):
        super().__init__()
        self.network = torch.nn.Sequential(
            torch.nn.LazyLinear(128),
            torch.nn.LeakyReLU(),
            torch.nn.Dropout(p_dropout),
            torch.nn.LazyLinear(128),
            torch.nn.LeakyReLU(),
            torch.nn.Dropout(p_dropout),
            torch.nn.LazyLinear(128),
            torch.nn.LeakyReLU(),
            torch.nn.Dropout(p_dropout),
            torch.nn.LazyLinear(output),
        )

    def forward(self, input_: torch.tensor):
        return self.network(input_)


class Dataset(torch.utils.data.Dataset):
    """The Dataset is responsible for accessing and processing single instances of data"""

    def __init__(self, words: torch.tensor, labels: torch.tensor):
        self.words = words
        self.labels = labels

    def __len__(self):
        return self.labels.size(0)

    def __getitem__(self, idx: int):
        return self.words[idx, :], self.labels[idx, :]
