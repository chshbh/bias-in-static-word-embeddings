# system setup
    # system info : Linux chshbh 5.10.0-14-amd64 #1 SMP Debian 5.10.113-1 (2022-04-29) x86_64 GNU/Linux
    # update system
    sudo apt update && sudo apt -y full-upgrade
    # [install pip](https://pip.pypa.io/en/stable/installation/)
    sudo apt install python3-pip
    sudo -H pip3 install --upgrade pip
    pip --version
    # [install pipx](https://pypa.github.io/pipx/installation/)
    sudo apt install pipx
    or  python3 -m pip install --user pipx
        python3 -m pipx ensurepath
    # [install pdm](https://pdm.fming.dev/)
    pipx install pdm
    
# dependency management
    pdm init -vv
    pdm install && pdm sync
    note : requirement.txt and setup.py are generated using pdm, using it on other platforms may cause unexpected result
