import numpy as np
import torch
import torchtext
import torchmetrics
from operator import add, sub, mul, truediv, floordiv
from json import loads


### WORD EMBEDDING ###
EMBEDDING_PATH = "glove.840B.300d.txt"
# EMBEDDING_PATH = "GoogleNews-vectors-negative300.txt"
CACHE_PATH = "../.vector_cache"
word_embedd = torchtext.vocab.Vectors(EMBEDDING_PATH, CACHE_PATH)

cosine_similarity = lambda v1, v2: torchmetrics.functional.pairwise_cosine_similarity(
    v1.view(-1, 300), v2.view(-1, 300), reduction="mean"
)
split = lambda embedding, seeds, i: embedding.get_vecs_by_tokens(seeds[i])


def word_association(w, A, B):
    """
    returns difference in mean cosine similarity between a target word w and sets of attributes A, B
    s(w, A, B)
    s(W, A, B) = [s(w, A, B) for w in W]
    """

    return sub(
        (cosine_similarity(w, A)),
        (cosine_similarity(w, B)),
    )


def diff_association(X, Y, A, B):
    """
    returns the differential association of two sets of target words X, Y with the attribute
    s(X, Y, A, B)
    """

    return sub((word_association(X, A, B)).sum(), (word_association(Y, A, B)).sum())


def weat_score(X, Y, A, B):
    """
    returns weat score statistics
    """

    num = sub((word_association(X, A, B)).mean(), (word_association(Y, A, B)).mean())
    den = (torch.concat((word_association(X, A, B), word_association(Y, A, B)))).std()
    return truediv(num, den)


def weat_p_value(X, Y, A, B):
    """
    returns one-sided p-value of the permutation test
    """
    s = diff_association(X, Y, A, B)
    XY_union = torch.concat((X, Y))

    xuy = torch.zeros(len(XY_union), dtype=torch.bool)
    xuy[: floordiv(len(XY_union), 2)] = 1

    temp = list()
    for i in range(100):
        s_prime = list()
        for j in range(1000):
            idx = torch.randperm(xuy.shape[0], dtype=torch.long)
            part_x = XY_union[xuy[idx]]
            part_y = XY_union[xuy[~idx]]
            s_prime.append(diff_association(part_x, part_y, A, B))
        s_prime_T = torch.tensor(s_prime, dtype=torch.double)
        temp.append(truediv((s_prime_T >= s).sum().item(), len(s_prime_T)))
    return torch.tensor(temp).mean()


def wefat_score(W, A, B):
    """
    returns wefat normalized association score
    """
    num = word_association(W, A, B)
    den = (torch.concat((cosine_similarity(W, A), cosine_similarity(W, B)))).std()
    return (truediv(num, den)).mean()


def json_stimuli_2dict(filename):
    """
    returns dict of stimuli from json file
    """
    return loads(open(filename).read())


def dict_stimuli_choose_weat(list_of_stimuli, chosen_stimuli):
    """
    returns the selected stimuli from dict as tuple (X,Y,A,B)
    choose from :
        EuropeanAmerican_AfricanAmerican_Pleasant_Unpleasant
        EuropeanAmerican_AfricanAmerican_Pleasant_Unpleasant_2
        Flowers_Insects_Pleasant_Unpleasant
        Male_Female_Career_Family
        Math_Arts_Male_Female
        MusicalInstruments_Weapons_Pleasant_Unpleasant
        Science_Arts_Male_Female
    """
    temp = list_of_stimuli[chosen_stimuli]
    return (
        temp[temp["X_key"]],
        temp[temp["Y_key"]],
        temp[temp["A_key"]],
        temp[temp["B_key"]],
    )


def dict_stimuli_choose_wefat(list_of_stimuli, chosen_stimuli):
    """
    returns the selected stimuli from dict as tuple (W, A,B)
    choose from :
        Careers_Female_Male
        Names_Female_Male
    """
    temp = list_of_stimuli[chosen_stimuli]
    return (
        temp[temp["W_key"]],
        temp[temp["A_key"]],
        temp[temp["B_key"]],
    )


### preparing for weat ###
# X, Y are target words, A, B are attribute words
stimuli_weat = list()
stimuli_weat.append(
    dict_stimuli_choose_weat(
        json_stimuli_2dict("weat.json"), "Science_Arts_Male_Female"
    )
)
stimuli_weat.append(
    dict_stimuli_choose_weat(json_stimuli_2dict("weat.json"), "Math_Arts_Male_Female")
)
stimuli_weat.append(
    dict_stimuli_choose_weat(
        json_stimuli_2dict("weat.json"), "Male_Female_Career_Family"
    )
)
stimuli_weat.append(
    dict_stimuli_choose_weat(
        json_stimuli_2dict("weat.json"), "Flowers_Insects_Pleasant_Unpleasant"
    )
)


for i in stimuli_weat:
    X = split(word_embedd, i, 0)
    Y = split(word_embedd, i, 1)
    A = split(word_embedd, i, 2)
    B = split(word_embedd, i, 3)
    print(f"WEAT Score {weat_score(X,Y,A,B)}")
    print(f"WEAT p_value {weat_p_value(X,Y,A,B)}")


### preparing for wefat ###
# W is target word, A, B are attribute words
stimuli_wefat = list()
stimuli_wefat.append(
    dict_stimuli_choose_wefat(json_stimuli_2dict("weat.json"), "Careers_Female_Male")
)

for i in stimuli_wefat:
    W = split(word_embedd, i, 0)
    A = split(word_embedd, i, 0)
    B = split(word_embedd, i, 0)
    print(f"WEFAT Score {wefat_score(W,A,B)}")
