import numpy as np
from operator import add, sub, mul, truediv, floordiv
from functools import partial
from lib import unit_vector, load_embedding, test_stimuli, test_stimuli_dict_2array


def b_vector(x, y):
    """
    returns relation vector b
    """
    return unit_vector(sub(np.array(x), np.array(y)))


def ripa_score(b_vector, w):
    """
    returns dot product of word w with relation vector
    """
    return np.dot(b_vector, w)


### WORD EMBEDDING ###
# loading from self generated wikipedia first 100M dataset
word_embedd = load_embedding("vectors.txt")
# loading from pretrained dataset
# GLOVE
# FASTEXT
# word_embedd = load_embedding("crawl-300d-2M-subword.vec")
# WORD2VEC


# pairs = {('woman','man'), ('girl', 'boy'), ('she', 'he'), ('mother', 'father'), ('daughter', 'son'), ('gal', 'guy'), ('female', 'male'), ('her','his'), ('herself', 'himself'), ('mary', 'john')}
# female-male pair + family
target_set_1 = ["female", "woman", "girl", "sister", "she", "her", "hers", "daughter"]
target_set_2 = ["male", "man", "boy", "brother", "he", "him", "his", "son"]
attribute_set = [
    "mom",
    "king",
    "nurse",
    "architect",
    "cousins",
    "marriage",
    "wedding",
    "relatives",
]


test_stimuli = partial(test_stimuli, word_embedding=word_embedd)
attribute_embed = test_stimuli(attribute_set)
target_embed_1 = test_stimuli(target_set_1)
target_embed_2 = test_stimuli(target_set_2)


ripa_scores = dict()

# calculating the ripa score for each attribute word with each target pair
for i in attribute_set:
    ripa_scores[i] = []
    for j, k in zip(target_set_1, target_set_2):
        ripa_scores[i].append(
            ripa_score(
                b_vector(target_embed_1[j], target_embed_2[k]), attribute_embed[i]
            )
        )

ripa_oa_mean = []
ripa_oa_std = []

# calculating the mean of the ripa score across all target pairs for every attribute word
for i in attribute_set:
    ripa_oa_mean.append(np.mean(ripa_scores[i]))
    ripa_oa_std.append(np.std(ripa_scores[i]))


# creating a dictionary with the direction of the RIPA scores for every corresponding attribute word
word_values = {}
for i in range(len(ripa_oa_mean)):
    word_values[attribute_set[i]] = {
        "mean": ripa_oa_mean[i],
        "std": ripa_oa_std[i],
    }
